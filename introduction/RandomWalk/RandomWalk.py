import pyglet
from pyglet.ext import pgedraw
import random

class Walker(object):
    def __init__(self, width, height):
        self.x = width / 2.0
        self.y = width / 2.0

    def step(self):
        self.x += random.uniform(-1, 1)
        self.y += random.uniform(-1, 1)

    def draw(self):
        pyglet.gl.glColor4f(1.0,0,0,1.0)
        pyglet.graphics.draw(1, pyglet.gl.GL_POINTS, ('v2f', (self.x, self.y)))

window = pyglet.window.Window(resizable=True)
walker = Walker(window.width, window.height)

@window.event
def on_draw():
    walker.draw()
    walker.step()

if __name__ == '__main__':
    pyglet.app.run()

