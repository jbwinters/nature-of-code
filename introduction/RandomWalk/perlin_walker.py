import pyglet
from pyglet.ext import pgedraw
import noise
import random

class Walker(object):
    def __init__(self, width, height):
        self.x = width / 2.0
        self.y = width / 2.0
        self.time_x = 0.0
        self.time_y = 10000.0
        self.circle = pgedraw.Circle((self.x, self.y), 3)
        self.width = width
        self.height = height

    def step(self):
        translate_x = noise.pnoise1(self.time_x) * 5
        translate_y = noise.pnoise1(self.time_y) * 5
        self.circle.translate(translate_x, translate_y)
        self.x = (self.x + translate_x) % self.width
        self.y = (self.y + translate_y) % self.height
        self.time_x += 0.01
        self.time_y += 0.01

    def draw(self):
        self.circle.draw()
        #pyglet.gl.glColor4f(1.0,0,0,1.0)
        #pyglet.graphics.draw(1, pyglet.gl.GL_POINTS, ('v2f', (self.x, self.y)))

window = pyglet.window.Window(resizable=True)
walker = Walker(window.width, window.height)

@window.event
def on_draw():
    window.clear()
    walker.draw()
    walker.step()

if __name__ == '__main__':
    pyglet.app.run()

