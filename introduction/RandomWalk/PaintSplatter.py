import pyglet
import random
from math import sin, cos

class Walker(object):
    def __init__(self, width, height):
        self.start_x = width / 2.0
        self.start_y = width / 2.0
        self.x = self.start_x
        self.y = self.start_y
        self.angle = 0

    def step(self):
        dist = random.gauss(0, 50)
        self.x = self.start_x + dist * cos(self.angle)
        self.y = self.start_y + dist * sin(self.angle)
        self.angle = (self.angle + 1) % 360

    def draw(self):
        pyglet.gl.glColor4f(1.0,0,0,1.0)
        pyglet.graphics.draw(1, pyglet.gl.GL_POINTS, ('v2f', (self.x, self.y)))

window = pyglet.window.Window(resizable=True)
walker = Walker(window.width, window.height)

@window.event
def on_draw():
    walker.draw()
    walker.step()

if __name__ == '__main__':
    pyglet.app.run()

