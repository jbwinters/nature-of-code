import pyglet
import random

'''
Create a random walker with dynamic probabilities. For example, can you give it a 50% chance of moving in the direction of the mouse?
'''

class Walker(object):
    def __init__(self, width, height):
        self.x = width / 2.0
        self.y = width / 2.0
        self.target_x = width
        self.target_y = height

    def step(self):
        #self.x += random.uniform(-1, 1)
        #self.y += random.uniform(-1, 1)
        toward_target = random.random() >= 0.5

        x_step = random.random()
        if self.target_x < self.x + x_step:
            x_step = -x_step

        y_step = random.random()
        if self.target_y < self.y + y_step:
            y_step = -y_step

        self.x += x_step if toward_target else -x_step
        self.y += y_step if toward_target else -y_step


    def draw(self):
        pyglet.gl.glColor4f(1.0,0,0,1.0)
        pyglet.graphics.draw(1, pyglet.gl.GL_POINTS, ('v2f', (self.x, self.y)))

    def update_target(self, x, y):
        self.target_x = x
        self.target_y = y

window = pyglet.window.Window(resizable=True)
walker = Walker(window.width, window.height)

@window.event
def on_draw():
    walker.draw()
    walker.step()

@window.event
def on_mouse_motion(x, y, dx, dy):
    walker.update_target(x, y)

if __name__ == '__main__':
    pyglet.app.run()

