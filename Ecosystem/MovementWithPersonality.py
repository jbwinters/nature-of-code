import numpy as np
import pyglet
from pyglet.ext import pgedraw
import noise
import random
import math

class Walker(object):
    def __init__(self, width, height):
        self.location = np.array([width/2.0, height/2.0])
        self.direction = None
        self.circle = pgedraw.Circle((self.x, self.y), 3)
        self.width = width
        self.height = height
        self.target = None
        self.time = 0.0
        self.toward_target = None
        self.speed = 2.0

    @property
    def x(self):
        return self.location[0]
    @x.setter
    def x_set(self, value):
        self.location[0] = value

    @property
    def y(self):
        return self.location[1]
    @y.setter
    def y_set(self, value):
        self.location[1] = value

    def draw(self):
        self.circle.draw()

    def update_target(self, x, y):
        self.target = np.array([x, y])

    def step(self):
        raise NotImplementedError

class FollowMouse(Walker):
    def step(self):
        self.direction = normalize(self.target - self.location)

        x_step, y_step = self.direction
        self.circle.translate(x=x_step, y=y_step)
        self.location += self.direction

class Snake(Walker):
    def step(self):
        if self.target is None:
            return

        self.direction = normalize(self.target - self.location)
        freq = 8.0
        orth_amp = math.sin(freq*self.time) * 3
        orth = orthogonal(self.direction)
        orth_vec = orth * orth_amp

        step = normalize(self.direction*self.speed + orth_vec)
        self.circle.translate(x=step[0], y=step[1])
        self.location += step

        self.time += 0.01

class Rabbit(Walker):
    def __init__(self, *args, **kw):
        super(Rabbit, self).__init__(*args, **kw)
        self.speed = 2.0
        self.offset = 1.0
        self.offset_switch = False

    def step(self):
        if self.target is None:
            return
        if self.speed < self.time < self.speed + 1.5:
            self.time += 0.1
            return
        elif self.speed + 1.5 < self.time:
            self.time = 0.0
            self.offset_switch = not self.offset_switch
        self.direction = normalize(self.target - self.location)
        offset_vec = orthogonal(self.direction) * self.offset
        if self.offset_switch:
            offset_vec *= -1
        step = (self.direction * self.speed) + offset_vec

        self.location += step
        self.circle.translate(x=step[0], y=step[1])

        self.time += 0.1

def normalize(vector):
    return vector / np.linalg.norm(vector)

def orthogonal(vector):
    orth = np.array([-vector[1], vector[0]])
    return orth

window = pyglet.window.Window(resizable=True)
walkers = []
walkers.append(Rabbit(window.width, window.height))
walkers.append(Snake(window.width, window.height))

@window.event
def on_draw():
    window.clear()
    for walker in walkers:
        walker.draw()
        walker.step()


@window.event
def on_mouse_motion(x, y, dx, dy):
    for walker in walkers:
        walker.update_target(x, y)

if __name__ == '__main__':
    pyglet.app.run()

